# Cloud SQL Proxy Connector

This connector image is designed to run SQL queries against Cloud SQL through proxy during build process.

## Quickstart

Example use:

```bash
docker run --rm -ti \
    -v ${PWD}:/app \
    -e CLOUDSQL_PROJECT='my-first-project-12345' \ # your google cloud project
    -e CLOUDSQL_INSTANCE='my-first-project-12345:zone:db_name' \ # cloud sql instance (can be found in google cloud console)
    -e CLOUDSQL_CREDENTIALS='{JSON credentials file received from Google Cloud for a service account}' \
    registry.gitlab.com/gazanchyand/cloudsql-proxy-connector \
    node bin/migrate.js # any command you want to run that requires connection to your database
```

Another option to provide credentials information is through mounting credentials file and passing its path to `GCLOUD_CREDENTIALS_FILE` environment variable (handy when used with Gitlab CI file environment variable).
If none of the credential variables provided, then proxy will fall back to gcloud default authentication (handy in Cloud Build steps).

Database connection socket is available on `/cloudsql/my-first-project-12345:zone:db_name`,
where `my-first-project-12345:zone:db_name` is your cloud sql instance id.

## Licence

MIT
